package org.mik.hospital;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.mik.hospital.dao.HospitalDao;
import org.mik.hospital.department.Department;
import org.mik.hospital.disease.AbstractDisease;
import org.mik.hospital.disease.Disease;
import org.mik.hospital.disease.diagnosis.AbstractDiagnosis;
import org.mik.hospital.disease.diagnosis.Diagnosis;
import org.mik.hospital.disease.diagnosis.Seriousness;
import org.mik.hospital.disease.diagnosis.bacteria.Bacteria;
import org.mik.hospital.disease.diagnosis.injuries.Injuries;
import org.mik.hospital.disease.diagnosis.virus.Virus;
import org.mik.hospital.disease.symptoms.Symptoms;
import org.mik.hospital.person.AbstractPerson;
import org.mik.hospital.person.Adress;
import org.mik.hospital.person.BloodType;
import org.mik.hospital.person.Gender;
import org.mik.hospital.person.PatientType;
import org.mik.hospital.person.Person;
import org.mik.hospital.person.patient.Patient;
import org.mik.hospital.person.patient.PatientImpl;
import org.mik.hospital.person.staff.AbstractStaff;
import org.mik.hospital.person.staff.Staff;
import org.mik.hospital.person.staff.doctor.Doctor;
import org.mik.hospital.person.staff.doctor.ImplDoctor;
import org.mik.hospital.person.staff.doctor.Specialization;
import org.mik.hospital.person.staff.nurse.ImplNurse;
import org.mik.hospital.person.staff.nurse.Nurse;
import org.mik.hospital.person.staff.receptioner.ImplReceptioner;
import org.mik.hospital.person.staff.receptioner.Receptioner;
import org.mik.hospital.treatment.AbstractTreatment;
import org.mik.hospital.treatment.Treatment;
import org.mik.hospital.treatment.medicine.Medicine;
import org.mik.hospital.treatment.surgeon.Surgeon;

/**
 * Unit test for simple App.
 */
public class AppTest {
	private static final String VIRUS_NAME = "virusos"; //$NON-NLS-1$
	private static final Seriousness VIRUS_SERIOUSNESS = Seriousness.LEVEL_2;
	private static final Symptoms VIRUS_SYMPTOM = new Symptoms("eki"); //$NON-NLS-1$
	private static final List<Symptoms> VIRUS_SYMPTOMS = new ArrayList<Symptoms>();
	private static final String INJURY_NAME = "injuris"; //$NON-NLS-1$
	private static final Seriousness INJURY_SERIOUSNESS = Seriousness.LEVEL_1;
	private static final List<Symptoms> INJURY_SYMPTOMS = new ArrayList<Symptoms>();
	private static final String BACTERIA_NAME = "bacterias"; //$NON-NLS-1$
	private static final Seriousness BACTERIA_SERIOUSNESS = Seriousness.LEVEL_3;
	private static final List<Symptoms> BACTERIA_SYMPTOMS = new ArrayList<Symptoms>();
	private static final String ADRESS_COUNTRY = "Hungary"; //$NON-NLS-1$
	private static final String ADRESS_STATE = "Baranya"; //$NON-NLS-1$
	private static final String ADRESS_CITY = "Pécs"; //$NON-NLS-1$
	private static final Integer ADRESS_POSTALCODE = 3232;
	private static final String ADRESS_STREET = "Boszorkány"; //$NON-NLS-1$
	private static final LocalDate PATIENTBIRTDATE = LocalDate.of(1995, 12, 01);
	private static final String PATIENTFIRSTNAME = "CAITLYNN"; //$NON-NLS-1$
	private static final String PATIENTLASTNAME = "JENNER"; //$NON-NLS-1$
	private static final BloodType PATIENTBLOODTYPE = BloodType.A;
	private static final LocalDate PATIENTDATEOFDIAGNOSIS = LocalDate.now();
	private static final List<Symptoms> PATIENT_SYMPTOMS = new ArrayList<Symptoms>();
	private static final PatientType PATIENT_PATIENTTYPE = PatientType.INPATIENT;
	private static final LocalDate DOCTORBIRTDATE = LocalDate.of(1976, 10, 03);
	private static final String DOCTORFIRSTNAME = "Bruce"; //$NON-NLS-1$
	private static final String DOCTORLASTNAME = "Harper"; //$NON-NLS-1$
	private static final BloodType DOCTORBLOODTYPE = BloodType.O;
	private static final Integer DOCTORSALARY = 31223;
	private static final Specialization DOCTORSPECIALIZATION = Specialization.GENERAL_SURGEON;
	private static final LocalDate NURSEBIRTDATE = LocalDate.of(1975, 12, 01);
	private static final String NURSEFIRSTNAME = "Zach"; //$NON-NLS-1$
	private static final String NURSELASTNAME = "Taker"; //$NON-NLS-1$
	private static final BloodType NURSEBLOODTYPE = BloodType.AB;
	private static final Integer NURSESALARY = 35000;
	private static final Integer NURSENUMBEROFPATIENT = 5;
	private static final String MEDICINENAME = "Randommedicine"; //$NON-NLS-1$
	private static final Integer MEDICINEPRICE = 123213;
	private static final String SURGEONNAME = "Chestsurgeon"; //$NON-NLS-1$
	private static final String SURGEONBODYPART = "Chest"; //$NON-NLS-1$
	private static final String SYMPTOMS_NAME = "eki"; //$NON-NLS-1$

	// /**
	// * Create the test case
	// *
	// * @param testName
	// * name of the test case
	// */
	// public AppTest(String testName) {
	//
	// super(testName);
	//
	// Method[] a = App.class.getMethods();
	// System.out.println(a.toString());
	// }
	//
	// /**
	// * @return the suite of tests being tested
	// */
	// public static Test suite() {
	// return new TestSuite(AppTest.class);
	// }
	//
	// /**
	// * Rigourous Test :-)
	// */
	// public void testApp() {
	// assertTrue(true);
	// }
	// }
	@Test
	public void allTest() {
		classTest();
		objectTest();
		functionalTest();
	}

	/**
	 * 
	 */
	private void functionalTest() {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 */
	private void objectTest() {
		Virus v1 = new Virus(VIRUS_NAME, VIRUS_SERIOUSNESS, VIRUS_SYMPTOMS);
		assertNotNull(v1);
		assertEquals(v1.getName(), VIRUS_NAME);
		assertEquals(v1.getSeriousness(), VIRUS_SERIOUSNESS);
		assertTrue(VIRUS_SYMPTOMS.size() == 0);
		assertTrue(v1.isVirus());
		assertFalse(v1.isBacteria());
		assertFalse(v1.isDiagnosis());
		assertFalse(v1.isInjury());
		assertFalse(v1.isSymptom());
		Virus v2 = new Virus(VIRUS_NAME, VIRUS_SERIOUSNESS, VIRUS_SYMPTOMS);
		assertEquals(v1, v2);
		assertFalse(v1 == v2);

		Injuries i1 = new Injuries(INJURY_NAME, INJURY_SERIOUSNESS, INJURY_SYMPTOMS);
		assertNotNull(i1);
		assertEquals(i1.getName(), INJURY_NAME);
		assertEquals(i1.getSeriousness(), INJURY_SERIOUSNESS);
		assertTrue(INJURY_SYMPTOMS.size() == 0);
		assertFalse(i1.isVirus());
		assertFalse(i1.isBacteria());
		assertTrue(i1.isDiagnosis());
		assertTrue(i1.isInjury());
		assertFalse(i1.isSymptom());
		Injuries i2 = new Injuries(INJURY_NAME, INJURY_SERIOUSNESS, INJURY_SYMPTOMS);
		assertEquals(i1, i2);
		assertFalse(i1 == i2);

		Bacteria b1 = new Bacteria(BACTERIA_NAME, BACTERIA_SERIOUSNESS, BACTERIA_SYMPTOMS);
		assertNotNull(b1);
		assertEquals(b1.getName(), BACTERIA_NAME);
		assertEquals(b1.getSeriousness(), BACTERIA_SERIOUSNESS);
		assertTrue(BACTERIA_SYMPTOMS.size() == 0);
		assertFalse(b1.isVirus());
		assertTrue(b1.isBacteria());
		assertFalse(b1.isDiagnosis());
		assertFalse(b1.isInjury());
		assertFalse(b1.isSymptom());
		Bacteria b2 = new Bacteria(BACTERIA_NAME, BACTERIA_SERIOUSNESS, BACTERIA_SYMPTOMS);
		assertEquals(b1, b2);
		assertFalse(b1 == b2);

		Symptoms s1 = new Symptoms(SYMPTOMS_NAME);
		assertNotNull(s1);
		assertEquals(s1.getName(), SYMPTOMS_NAME);
		assertFalse(s1.isVirus());
		assertFalse(s1.isBacteria());
		assertFalse(s1.isDiagnosis());
		assertFalse(s1.isInjury());
		assertTrue(s1.isSymptom());
		Symptoms s2 = new Symptoms(SYMPTOMS_NAME);
		assertEquals(s1, s2);
		assertFalse(s1 == s2);

		Adress a1 = new Adress(ADRESS_COUNTRY, ADRESS_STATE, ADRESS_CITY, ADRESS_POSTALCODE, ADRESS_STREET);
		assertNotNull(a1);
		assertEquals(a1.getCountry(), ADRESS_COUNTRY);
		assertEquals(a1.getState(), ADRESS_STATE);
		assertEquals(a1.getCity(), ADRESS_CITY);
		assertEquals(a1.getPostalCode(), ADRESS_POSTALCODE);
		assertEquals(a1.getStreet(), ADRESS_STREET);
		Adress a2 = new Adress(ADRESS_COUNTRY, ADRESS_STATE, ADRESS_CITY, ADRESS_POSTALCODE, ADRESS_STREET);
		assertEquals(a1, a2);
		assertFalse(a1 == a2);

		Patient p1 = new PatientImpl(a1, PATIENTBIRTDATE, PATIENTFIRSTNAME, PATIENTLASTNAME, PATIENTBLOODTYPE,
				PATIENTDATEOFDIAGNOSIS, PATIENT_SYMPTOMS, PATIENT_PATIENTTYPE);
		assertNotNull(p1);
		assertEquals(p1.getAdress(), a1);
		assertEquals(p1.getBirtDate(), PATIENTBIRTDATE);
		assertEquals(p1.getFirstName(), PATIENTFIRSTNAME);
		assertEquals(p1.getLastName(), PATIENTLASTNAME);
		assertEquals(p1.getBloodType(), PATIENTBLOODTYPE);
		assertEquals(p1.getDateOfDiagnosis(), PATIENTDATEOFDIAGNOSIS);
		assertTrue(PATIENT_SYMPTOMS.size() == 0);
		assertFalse(p1.isDoctor());
		assertFalse(p1.isNurse());
		assertFalse(p1.isReceptionist());
		assertTrue(p1.isPatient());
		assertFalse(p1.isStaff());
		assertEquals(p1.getPatientType(), PATIENT_PATIENTTYPE);
		Patient p2 = new PatientImpl(a1, PATIENTBIRTDATE, PATIENTFIRSTNAME, PATIENTLASTNAME, PATIENTBLOODTYPE,
				PATIENTDATEOFDIAGNOSIS, PATIENT_SYMPTOMS, PATIENT_PATIENTTYPE);
		assertEquals(p1, p2);
		assertFalse(p1 == p2);

		Doctor d1 = new ImplDoctor(a1, DOCTORBIRTDATE, DOCTORFIRSTNAME, DOCTORLASTNAME, DOCTORBLOODTYPE, DOCTORSALARY,
				DOCTORSPECIALIZATION);
		assertEquals(d1.getAdress(), a1);
		assertEquals(d1.getBirtDate(), DOCTORBIRTDATE);
		assertEquals(d1.getFirstName(), DOCTORFIRSTNAME);
		assertEquals(d1.getLastName(), DOCTORLASTNAME);
		assertEquals(d1.getBloodType(), DOCTORBLOODTYPE);
		assertEquals(d1.getSalary(), DOCTORSALARY);
		assertEquals(d1.getSpecialization(), DOCTORSPECIALIZATION);
		assertTrue(d1.isDoctor());
		assertFalse(d1.isNurse());
		assertFalse(d1.isReceptionist());
		assertFalse(d1.isPatient());
		assertTrue(d1.isStaff());
		Doctor d2 = new ImplDoctor(a1, DOCTORBIRTDATE, DOCTORFIRSTNAME, DOCTORLASTNAME, DOCTORBLOODTYPE, DOCTORSALARY,
				DOCTORSPECIALIZATION);
		assertEquals(d1, d2);
		assertFalse(d1 == d2);

		Nurse n1 = new ImplNurse(a2, NURSEBIRTDATE, NURSEFIRSTNAME, NURSELASTNAME, NURSEBLOODTYPE, NURSESALARY,
				NURSENUMBEROFPATIENT);
		assertEquals(n1.getAdress(), a2);
		assertEquals(n1.getBirtDate(), NURSEBIRTDATE);
		assertEquals(n1.getFirstName(), NURSEFIRSTNAME);
		assertEquals(n1.getLastName(), NURSELASTNAME);
		assertEquals(n1.getBloodType(), NURSEBLOODTYPE);
		assertEquals(n1.getSalary(), NURSESALARY);
		assertEquals(n1.getNumberOfPatient(), NURSENUMBEROFPATIENT);
		assertFalse(n1.isDoctor());
		assertTrue(n1.isNurse());
		assertFalse(n1.isReceptionist());
		assertFalse(n1.isPatient());
		assertTrue(n1.isStaff());
		Nurse n2 = new ImplNurse(a2, NURSEBIRTDATE, NURSEFIRSTNAME, NURSELASTNAME, NURSEBLOODTYPE, NURSESALARY,
				NURSENUMBEROFPATIENT);
		assertEquals(n1, n2);
		assertFalse(n1 == n2);

		Medicine m1 = new Medicine(MEDICINENAME, MEDICINEPRICE);
		assertEquals(m1.getName(), MEDICINENAME);
		assertEquals(m1.getPrice(), MEDICINEPRICE);
		assertTrue(m1.isMedicine());
		assertFalse(m1.isSurgeon());
		Medicine m2 = new Medicine(MEDICINENAME, MEDICINEPRICE);
		assertEquals(m1, m2);
		assertFalse(m1 == m2);

		Surgeon su1 = new Surgeon(SURGEONNAME, SURGEONBODYPART);
		assertEquals(su1.getName(), SURGEONNAME);
		assertEquals(su1.getBodyPart(), SURGEONBODYPART);
		assertFalse(su1.isMedicine());
		assertTrue(su1.isSurgeon());
		Surgeon su2 = new Surgeon(SURGEONNAME, SURGEONBODYPART);
		assertEquals(su1, su2);
		assertFalse(su1 == su2);

	}

	/**
	 * 
	 */
	private void classTest() {
		try {
			assertTrue(Disease.class.isInterface());
			Method m = Disease.class.getMethod("isBacteria"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = Disease.class.getMethod("isInjury"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = Disease.class.getMethod("isVirus"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = Disease.class.getMethod("isDiagnosis"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = Disease.class.getMethod("isSymptom"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = Disease.class.getMethod("getName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			assertTrue(Modifier.isAbstract(AbstractDisease.class.getModifiers()));
			assertTrue(Arrays.asList(AbstractDisease.class.getInterfaces()).contains(Disease.class));

			m = AbstractDisease.class.getMethod("getName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			m = AbstractDisease.class.getMethod("setName", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);

			m = AbstractDisease.class.getMethod("isBacteria"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = AbstractDisease.class.getMethod("isInjury"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = AbstractDisease.class.getMethod("isVirus"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = AbstractDisease.class.getMethod("isDiagnosis"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = AbstractDisease.class.getMethod("isSymptom"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = AbstractDisease.class.getMethod("toString"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			Constructor<?> c = AbstractDisease.class.getConstructor(String.class);
			assertNotNull(c);

			assertTrue(Diagnosis.class.isInterface());

			m = Diagnosis.class.getMethod("getSeriousness"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Seriousness.class);

			assertTrue(Modifier.isAbstract(AbstractDiagnosis.class.getModifiers()));
			assertTrue(Arrays.asList(AbstractDiagnosis.class.getInterfaces()).contains(Diagnosis.class));

			m = AbstractDiagnosis.class.getMethod("getSeriousness"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Seriousness.class);

			m = AbstractDiagnosis.class.getMethod("setSeriousness", Seriousness.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);

			m = AbstractDiagnosis.class.getMethod("getSymptoms"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), List.class);

			m = AbstractDiagnosis.class.getMethod("toString"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			c = AbstractDiagnosis.class.getConstructor(String.class, Seriousness.class, List.class);
			assertNotNull(c);

			m = Bacteria.class.getMethod("isBacteria"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = Bacteria.class.getMethod("toString"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			c = Bacteria.class.getConstructor(String.class, Seriousness.class, List.class);
			assertNotNull(c);

			m = Injuries.class.getMethod("isInjury"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			m = Injuries.class.getMethod("toString"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			c = Injuries.class.getConstructor(String.class, Seriousness.class, List.class);
			assertNotNull(c);

			m = Virus.class.getMethod("isVirus"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = Virus.class.getMethod("toString"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			c = Virus.class.getConstructor(String.class, Seriousness.class, List.class);
			assertNotNull(c);

			m = Symptoms.class.getMethod("isSymptom"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = Symptoms.class.getMethod("toString"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			assertTrue(Person.class.isInterface());
			m = Person.class.getMethod("getAdress"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Adress.class);
			m = Person.class.getMethod("getBirtDate"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), LocalDate.class);
			m = Person.class.getMethod("getFullName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Person.class.getMethod("getFirstName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Person.class.getMethod("getLastName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Person.class.getMethod("isDoctor"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = Person.class.getMethod("isStaff"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = Person.class.getMethod("isNurse"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = Person.class.getMethod("isReceptionist"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = Person.class.getMethod("isPatient"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			assertTrue(BloodType.class.isEnum());
			assertTrue(Gender.class.isEnum());
			assertTrue(PatientType.class.isEnum());

			assertTrue(Modifier.isAbstract(AbstractPerson.class.getModifiers()));
			assertTrue(Arrays.asList(AbstractPerson.class.getInterfaces()).contains(Person.class));

			m = AbstractPerson.class.getMethod("getBloodType"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), BloodType.class);
			m = AbstractPerson.class.getMethod("setBloodType", BloodType.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = AbstractPerson.class.getMethod("getAdress"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Adress.class);
			m = AbstractPerson.class.getMethod("setAdress", Adress.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = AbstractPerson.class.getMethod("getBirtDate"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), LocalDate.class);
			m = AbstractPerson.class.getMethod("setBirtDate", LocalDate.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = AbstractPerson.class.getMethod("getFirstName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = AbstractPerson.class.getMethod("setFirstName", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = AbstractPerson.class.getMethod("getLastName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = AbstractPerson.class.getMethod("setLastName", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = AbstractPerson.class.getMethod("getFullname"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = AbstractPerson.class.getMethod("isDoctor"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = AbstractPerson.class.getMethod("isStaff"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = AbstractPerson.class.getMethod("isNurse"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			;
			m = AbstractPerson.class.getMethod("isReceptionist"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = AbstractPerson.class.getMethod("isPatient"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = AbstractPerson.class.getMethod("toString"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);

			m = Adress.class.getMethod("getCountry"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Adress.class.getMethod("setCountry", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = Adress.class.getMethod("getState"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Adress.class.getMethod("setState", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = Adress.class.getMethod("getCity"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Adress.class.getMethod("setCity", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = Adress.class.getMethod("getPostalCode"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Adress.class.getMethod("setPostalCode", Integer.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = Adress.class.getMethod("getStreet"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Adress.class.getMethod("setStreet", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			c = Adress.class.getConstructor(String.class, String.class, String.class, Integer.class, String.class);
			assertNotNull(c);

			assertTrue(Patient.class.isInterface());
			m = Patient.class.getMethod("getDateOfDiagnosis"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), LocalDate.class);

			m = PatientImpl.class.getMethod("getDateOfDiagnosis"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), LocalDate.class);
			m = PatientImpl.class.getMethod("setDateOfDiagnosis", LocalDate.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = PatientImpl.class.getMethod("getSymptoms"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), List.class);
			m = PatientImpl.class.getMethod("setSymptoms", List.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = PatientImpl.class.getMethod("isPatient"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			assertTrue(Staff.class.isInterface());
			m = Staff.class.getMethod("getSalary"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Integer.class);

			assertTrue(Modifier.isAbstract(AbstractStaff.class.getModifiers()));
			assertTrue(Arrays.asList(AbstractStaff.class.getInterfaces()).contains(Staff.class));
			m = AbstractStaff.class.getMethod("getSalary"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Integer.class);
			m = AbstractStaff.class.getMethod("setSalary", Integer.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = AbstractStaff.class.getMethod("isStaff"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			c = AbstractStaff.class.getConstructor(Adress.class, LocalDate.class, String.class, String.class,
					BloodType.class, Integer.class);
			assertNotNull(c);

			assertTrue(Receptioner.class.isInterface());
			m = Receptioner.class.getMethod("getPhoneNumber"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Integer.class);

			m = ImplReceptioner.class.getMethod("getPhoneNumber"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Integer.class);
			m = ImplReceptioner.class.getMethod("setPhoneNumber", Integer.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = ImplReceptioner.class.getMethod("isReceptionist"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			c = ImplReceptioner.class.getConstructor(Adress.class, LocalDate.class, String.class, String.class,
					BloodType.class, Integer.class, Integer.class);
			assertNotNull(c);

			assertTrue(Nurse.class.isInterface());
			m = Nurse.class.getMethod("getNumberOfPatient"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Integer.class);

			m = ImplNurse.class.getMethod("getNumberOfPatient"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Integer.class);
			m = ImplNurse.class.getMethod("setNumberOfPatient", Integer.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = ImplNurse.class.getMethod("isNurse"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			c = ImplNurse.class.getConstructor(Adress.class, LocalDate.class, String.class, String.class,
					BloodType.class, Integer.class, Integer.class);
			assertNotNull(c);
			assertTrue(Specialization.class.isEnum());

			assertTrue(Doctor.class.isInterface());
			m = Doctor.class.getMethod("getSpecialization"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Specialization.class);

			m = ImplDoctor.class.getMethod("getSpecialization"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Specialization.class);
			m = ImplDoctor.class.getMethod("setSpecialization", Specialization.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = ImplDoctor.class.getMethod("isDoctor"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			c = ImplDoctor.class.getConstructor(Adress.class, LocalDate.class, String.class, String.class,
					BloodType.class, Integer.class, Specialization.class);
			assertNotNull(c);

			assertTrue(Treatment.class.isInterface());
			m = Treatment.class.getMethod("getName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Treatment.class.getMethod("isSurgeon"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = Treatment.class.getMethod("isMedicine"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);

			assertTrue(Modifier.isAbstract(AbstractTreatment.class.getModifiers()));
			assertTrue(Arrays.asList(AbstractTreatment.class.getInterfaces()).contains(Treatment.class));
			m = AbstractTreatment.class.getMethod("getName"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = AbstractTreatment.class.getMethod("setName", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = AbstractTreatment.class.getMethod("isSurgeon"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			m = AbstractTreatment.class.getMethod("isMedicine"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			c = AbstractTreatment.class.getConstructor(String.class);

			m = Surgeon.class.getMethod("getBodyPart"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), String.class);
			m = Surgeon.class.getMethod("setBodyPart", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = Surgeon.class.getMethod("isSurgeon"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			c = Surgeon.class.getConstructor(String.class, String.class);
			assertNotNull(c);

			m = Medicine.class.getMethod("getPrice"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Integer.class);
			m = Medicine.class.getMethod("setPrice", Integer.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = Medicine.class.getMethod("isMedicine"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), boolean.class);
			c = Medicine.class.getConstructor(String.class, Integer.class);

			assertTrue(HospitalDao.class.isInterface());
			// TODO
			m = HospitalDao.class.getMethod("findAllSymptoms"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = HospitalDao.class.getMethod("findAllDiagnosis"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = HospitalDao.class.getMethod("findAllPerson", String.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = HospitalDao.class.getMethod("findAllTreatment"); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), void.class);
			m = HospitalDao.class.getMethod("findPatientwithDisease", Diagnosis.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Patient.class);
			m = HospitalDao.class.getMethod("findDiseasewithSymptoms", List.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Disease.class);
			m = HospitalDao.class.getMethod("findTreatmentforDiagnosis", Diagnosis.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Treatment.class);
			m = HospitalDao.class.getMethod("findDepoartmentforPatient", Patient.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Department.class);
			m = HospitalDao.class.getMethod("findDepoartmentforTreatment", Treatment.class); //$NON-NLS-1$
			assertNotNull(m);
			assertEquals(m.getReturnType(), Department.class);
			// TODO
			// m=HospitalDao.class.getMethod("findPersonsFromDepartment",
			// Type.class,Department.class);
			// assertNotNull(m);
			// assertEquals(m.getReturnType(),List.class);}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
