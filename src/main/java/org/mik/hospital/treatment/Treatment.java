/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.treatment;

/**
 * Interface for Treatment , constans for databases
 *
 * @author Burka Benjamin
 *
 */
public interface Treatment {
	public final static String TABLE_NAME = "treatment";
	public final static String COL_ID = "id";
	public final static String COL_NAME = "name";
	public final static String COL_PRICE = "price";
	public final static String COL_BODY_PART = "body_part";
	public final static String COL_DEPARTMENT_ID = "department_id";
	public final static String COL_DIAGNOSIS_ID = "diagnosis id";

	public String getName();

	public boolean isSurgeon();

	public boolean isMedicine();

}
