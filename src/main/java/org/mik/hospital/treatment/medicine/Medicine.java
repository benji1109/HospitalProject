/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.treatment.medicine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.treatment.AbstractTreatment;

/**
 * final class Medicine
 *
 * @author Burka Benjamin
 */
public final class Medicine extends AbstractTreatment {
	private Integer price;
	public static List<Medicine> medicines = new ArrayList<>();

	/**
	 * Constructor hozzáadja a medicines listához
	 *
	 * @param name
	 * @param price
	 */
	public Medicine(String name, Integer price) {
		super(name);
		this.price = price;
		medicines.add(this);
	}

	/**
	 * Resultsetes constructor
	 *
	 * @param rs
	 * @throws SQLException
	 */
	public Medicine(ResultSet rs) throws SQLException {
		super(rs);
		this.price = Integer.valueOf(rs.getInt(Medicine.COL_PRICE));
		medicines.add(this);
	}

	/**
	 * Integer típusban visszaadja az árat
	 *
	 * @return
	 */
	public Integer getPrice() {
		return this.price;
	}

	/**
	 * Price to set
	 *
	 * @param price
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.price == null) ? 0 : this.price.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Medicine other = (Medicine) obj;
		if (this.price == null) {
			if (other.price != null) {
				return false;
			}
		} else if (!this.price.equals(other.price)) {
			return false;
		}
		return true;
	}

	/**
	 * felülírja az isMedicine() függvényt
	 */
	@Override
	public boolean isMedicine() {
		return true;
	}

	// TODO tostring

}
