/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.treatment.surgeon;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.treatment.AbstractTreatment;

/**
 * final class Surgeon
 *
 * @author Burka Benjamin
 */
public final class Surgeon extends AbstractTreatment {

	public String bodyPart;
	public static List<Surgeon> surgeons = new ArrayList<>();

	/**
	 * Konstruktor , a végén listához adja
	 *
	 * @param name
	 * @param bodyPart
	 */
	public Surgeon(String name, String bodyPart) {
		super(name);
		this.bodyPart = bodyPart;
		surgeons.add(this);
	}

	/**
	 * Resultsetes konstruktor
	 *
	 * @param rs
	 * @throws SQLException
	 */
	public Surgeon(ResultSet rs) throws SQLException {
		super(rs);
		this.bodyPart = rs.getString(Surgeon.COL_BODY_PART);
		surgeons.add(this);
	}

	/**
	 * visszaadja a testrészt
	 *
	 * @return String
	 */
	public String getBodyPart() {
		return this.bodyPart;
	}

	/**
	 * bodyPart to set
	 *
	 * @param bodyPart
	 */
	public void setBodyPart(String bodyPart) {
		this.bodyPart = bodyPart;
	}

	/**
	 * felülírja az isSurgeon metódust , az értékét igazra állítva
	 */
	@Override
	public boolean isSurgeon() {
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.bodyPart == null) ? 0 : this.bodyPart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Surgeon other = (Surgeon) obj;
		if (this.bodyPart == null) {
			if (other.bodyPart != null) {
				return false;
			}
		} else if (!this.bodyPart.equals(other.bodyPart)) {
			return false;
		}
		return true;
	}

}
