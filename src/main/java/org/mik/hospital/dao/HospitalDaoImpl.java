/**
 *
 */
package org.mik.hospital.dao;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.department.Department;
import org.mik.hospital.disease.diagnosis.Diagnosis;
import org.mik.hospital.disease.diagnosis.bacteria.Bacteria;
import org.mik.hospital.disease.diagnosis.injuries.Injuries;
import org.mik.hospital.disease.diagnosis.virus.Virus;
import org.mik.hospital.disease.symptoms.Symptoms;
import org.mik.hospital.person.AbstractPerson;
import org.mik.hospital.person.Adress;
import org.mik.hospital.person.Person;
import org.mik.hospital.person.patient.Patient;
import org.mik.hospital.person.patient.PatientImpl;
import org.mik.hospital.person.staff.Staff;
import org.mik.hospital.person.staff.doctor.ImplDoctor;
import org.mik.hospital.person.staff.nurse.ImplNurse;
import org.mik.hospital.person.staff.receptioner.ImplReceptioner;
import org.mik.hospital.treatment.AbstractTreatment;
import org.mik.hospital.treatment.Treatment;
import org.mik.hospital.treatment.medicine.Medicine;
import org.mik.hospital.treatment.surgeon.Surgeon;

import com.mysql.cj.api.jdbc.Statement;
import com.mysql.cj.jdbc.PreparedStatement;

/**
 * @author Burka Benjamin
 *
 */
public class HospitalDaoImpl implements HospitalDao {

	private static HospitalDaoImpl instance = null;

	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	private HospitalDaoImpl() {

	}

	public static synchronized HospitalDaoImpl getInstance() {
		if (instance == null) {
			instance = new HospitalDaoImpl();
		}
		return instance;
	}

	public void readDataBase() throws Exception {
		try {
			this.createConnection();
			// this.restoreDB();
			this.fillUpFromDB();
		} catch (Exception e) {
			throw e;
		} finally {
			this.close();
		}
	}

	public boolean backupDB() throws Exception {
		Path p = Paths.get(PATH);
		String path = p.toString() + "\\" + BACKUP_SQL;
		String executeCmd = "mysqldump -u " + USERNAME + " --add-drop-database -B " + DATABASE + " -r " + path;
		Process runtimeProcess;
		try {
			runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();

			if (processComplete == 0) {
				System.out.println("Backup created successfully");
				return true;
			} else {
				System.out.println("Could not create the backup");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public void createConnection() throws ClassNotFoundException, SQLException {
		Class.forName(MYSQL_DRIVER);
		this.connect = DriverManager.getConnection(URL_AND_PORT, USERNAME, PASSWORD);
	}

	public void fillUpFromDB() throws Exception {
		this.findAllDiagnosis();
		this.findAllSymptoms();
		this.findAllTreatment();
		this.findAllAdress();
		this.findAllPerson();
		this.findAllDepartment();
		this.fillUpDepartment();
	}

	private void findAllDepartment() throws SQLException {
		this.statement = (Statement) this.connect.createStatement();
		String sql = "select * from " + Department.TABLE_NAME;
		this.resultSet = this.statement.executeQuery(sql);
		while (this.resultSet.next()) {
			new Department(this.resultSet);
		}
	}

	public void deletePersonById(int id) throws SQLException {
		this.statement = (Statement) this.connect.createStatement();
		String sql = "DELETE FROM person WHERE id = " + id;
		this.statement.executeUpdate(sql);

	}

	private void findAllAdress() throws SQLException {
		this.statement = (Statement) this.connect.createStatement();
		String sql = "select * from " + Adress.TABLE_NAME;
		this.resultSet = this.statement.executeQuery(sql);
		while (this.resultSet.next()) {
			new Adress(this.resultSet);
		}

	}

	@Override
	public void findAllSymptoms() throws SQLException {
		this.statement = (Statement) this.connect.createStatement();
		String sql = "select * from " + Symptoms.TABLE_NAME;
		this.resultSet = this.statement.executeQuery(sql);
		while (this.resultSet.next()) {
			new Symptoms(this.resultSet.getString(Symptoms.COL_NAME));
		}
	}

	@Override
	public void findAllDiagnosis() throws SQLException {
		// this.statement = (Statement) this.connect.createStatement();
		// String sql = "SELECT diagnosis.name as diagnosisname
		// ,diagnosis.type,diagnosis.id as id,symptoms.name as symptomname ,
		// diagnosis.seriousness as seriousness from diagnosis_symptoms INNER JOIN
		// diagnosis on diagnosis.id=diagnosis_symptoms.diagnosis_id INNER JOIN symptoms
		// on symptoms.id=diagnosis_symptoms.symptoms_id";
		// this.resultSet = this.statement.executeQuery(sql);
		String sql2 = "SELECT COUNT(*) as db,diagnosis.name as name,diagnosis.id as id,symptoms.name as sname,diagnosis.type, diagnosis.seriousness from diagnosis_symptoms INNER JOIN diagnosis on diagnosis.id=diagnosis_symptoms.diagnosis_id INNER JOIN symptoms on symptoms.id=diagnosis_symptoms.symptoms_id GROUP BY diagnosis.name";
		Statement statement = (Statement) this.connect.createStatement();
		ResultSet rSet = statement.executeQuery(sql2);

		while (rSet.next()) {
			if (rSet.getInt("db") == 1) {
				this.createDiagnosis(rSet, null);
			} else {
				this.findByDiagnosisId(rSet.getInt("id"));
			}
		}
	}

	public void findByDiagnosisId(int id) throws SQLException {
		List<Symptoms> list = new ArrayList<>();
		String sql2 = "SELECT diagnosis.name as name,symptoms.name as sname,diagnosis.type as type, diagnosis.seriousness from diagnosis_symptoms INNER JOIN diagnosis on diagnosis.id=diagnosis_symptoms.diagnosis_id INNER JOIN symptoms on symptoms.id=diagnosis_symptoms.symptoms_id "
				+ "WHERE diagnosis.id=" + id;
		Statement statement = (Statement) this.connect.createStatement();
		ResultSet rSet = statement.executeQuery(sql2);

		while (rSet.next()) {
			list.add(new Symptoms(rSet.getString("sname")));
		}
		this.createDiagnosis(rSet, list);

	}

	public void createDiagnosis(ResultSet resultSet, List<Symptoms> list) throws SQLException {
		List<Symptoms> symptoms;
		if (list == null) {
			symptoms = new ArrayList<>();
		} else {
			symptoms = list;
			resultSet.beforeFirst();
			resultSet.next();
		}
		if (symptoms.size() == 0) {
			symptoms.add(new Symptoms(resultSet.getString("sname")));
		}
		switch (resultSet.getString("type")) {
		case VIRUS:
			new Virus(resultSet, symptoms);
			break;
		case BACTERIA:
			new Bacteria(resultSet, symptoms);
			break;
		case INJURY:
			new Injuries(resultSet, symptoms);
			break;
		default:
			break;
		}
	}

	@Override
	public void findAllTreatment() throws SQLException {
		this.statement = (Statement) this.connect.createStatement();
		String sql = "select * from " + Treatment.TABLE_NAME;
		this.resultSet = this.statement.executeQuery(sql);
		while (this.resultSet.next()) {
			if (this.resultSet.getInt(Treatment.COL_PRICE) != 0) {
				new Medicine(this.resultSet);
			} else {
				new Surgeon(this.resultSet);
			}
		}
	}

	public Adress searchAddress(ResultSet resultSets) {
		return null;
	}

	// public ResultSet findAllPersons() throws SQLException {
	// this.statement = (Statement) this.connect.createStatement();
	// String sql = "SELECT id,first_name,last_name,bloodtype,birth_date FROM " +
	// Person.TABLE_NAME;
	// this.resultSet = this.statement.executeQuery(sql);
	// return this.resultSet;
	// }

	@Override
	public void findAllPerson() throws Exception {
		this.statement = (Statement) this.connect.createStatement();
		String sql = "SELECT * FROM " + Person.TABLE_NAME + " p INNER JOIN " + Adress.TABLE_NAME
				+ " a ON a.id=p.adress_id";
		this.resultSet = this.statement.executeQuery(sql);
		while (this.resultSet.next()) {
			if (this.resultSet.getInt(Person.COL_SALARY) == 0) {
				new PatientImpl(this.resultSet, new Adress(this.resultSet));
			}
			if (this.resultSet.getInt(Person.COL_PHONE_NUMBER) != 0) {
				new ImplReceptioner(this.resultSet, new Adress(this.resultSet));
			}
			if (this.resultSet.getInt(Person.COL_SPECIALIZATION) != 0) {
				new ImplDoctor(this.resultSet, new Adress(this.resultSet));
			}
			if (this.resultSet.getInt(Person.COL_NUMBER_OF_PATIENT) != 0) {
				new ImplNurse(this.resultSet, new Adress(this.resultSet));
			}
		}
	}

	@Override
	public Patient findPatientwithDisease(Diagnosis diagnosis) throws SQLException {
		// Personnak nincs slaaryja és a department idja meg egyezik
		// SQL SELECT * FROM person p
		/*
		 * SELECT * FROM person p INNER JOIN department d on d.id=p.department_id INNER
		 * JOIN treatment t on t.department_id=d.id INNER JOIN diagnosis di on
		 * di.treatment_id=t.id WHERE di.name LIKE '%rák%' AND (salary is null or salary
		 * ==0)
		 */
		// if van találat -- first name lastname egyezés
		return null;
	}

	@Override
	public Diagnosis findDiagnosiswithSymptoms(List<Symptoms> symptoms) throws SQLException {
		/*
		 *
		 * search in AllDisease
		 *
		 *
		 *
		 */
		// for (Diagnosis d : AbstractDiagnosis.alldiagnosis) {
		// if (d.getSymptoms().equals(symptoms)) {
		// return d;
		// }
		// }

		return null;
	}

	@Override
	public Treatment findTreatmentforDiagnosis(Diagnosis diagnosis) throws SQLException {
		// SELECT t.id FROM treatment t
		// INNER JOIN diagnosis di on di.treatment_id=t.id where ? -->
		// diagnosis.name=objektum
		/*
		 * SELECT * FROM treatment t where t.id=?
		 *
		 * return new treatmentfromrs // VAGY KERESSE KI A MAPOBJEKTUMBÓL MÁR
		 */
		return null;
	}

	@Override
	public Department findDepartmentforPatient(Patient patient) throws SQLException {

		/*
		 *
		 * SELECT * FROM department d where d.name=?
		 *
		 *
		 * searchinMAP -->
		 *
		 * return new treatmentfromrs // VAGY KERESSE KI A MAPOBJEKTUMBÓL MÁR
		 */
		return null;
	}

	@Override
	public Department findDepartmentforTreatment(Treatment treatment) throws SQLException {
		/*
		 * String name SELECT department.name FROM treatment t INNER JOIN department d
		 * on d.id=t.department_id where t.name=?-->treatment.name
		 *
		 * searchInAllDepartment
		 *
		 */
		return null;
	}

	public void fillUpDepartment() throws SQLException {
		this.personForDepartment();
		this.treatmentForDepartment();
	}

	private void treatmentForDepartment() throws SQLException {
		String sql = "SELECT COUNT(D.name) as DB,D.name as name,t.name as tname,t.id as tid,D.id as did FROM department D INNER JOIN treatment t ON t.department_id=D.id GROUP BY D.name";
		Statement sts = (Statement) this.connect.createStatement();
		ResultSet rs = sts.executeQuery(sql);
		while (rs.next()) {
			if (rs.getInt("DB") == 1) {
				this.createDepartmentForTreatment(rs);
			} else {
				this.findByTreatmentId(rs.getInt("did"));
			}
		}
	}

	private void findByTreatmentId(int id) throws SQLException {
		String sql = "SELECT t.name as tname,t.id as tid,D.id as did FROM department D INNER JOIN treatment t ON t.department_id=D.id where D.id="
				+ id;
		Statement statement = (Statement) this.connect.createStatement();
		ResultSet resultSet = this.statement.executeQuery(sql);
		List<Treatment> treatments = new ArrayList<>();
		while (resultSet.next()) {
			for (Treatment treatment : AbstractTreatment.allTreatments) {
				if (treatment.getName().equals(resultSet.getString("tname"))) {
					treatments.add(treatment);
				}
			}
		}
		this.findDepartmentById(id).setTreatments(treatments);
		;
	}

	private Department findDepartmentById(int id) throws SQLException {
		String sql = "SELECT t.name as tname,t.id as tid,D.id as did,D.name as dname FROM department D INNER JOIN treatment t ON t.department_id=D.id where D.id="
				+ id + " group by dname";
		Statement statement = (Statement) this.connect.createStatement();
		ResultSet resultSet = this.statement.executeQuery(sql);
		while (resultSet.next()) {
			for (Department d : Department.departments) {
				if (d.getName().equals(resultSet.getString("dname"))) {
					return d;
				}
			}
		}
		return null;
	}

	private void createDepartmentForTreatment(ResultSet resultSet2) throws SQLException {
		for (Department d : Department.departments) {
			if (d.getName().equals(resultSet2.getString("name"))) {
				this.searchTreatment(resultSet2.getString("tname"), d);
			}
		}
	}

	private void searchTreatment(String string, Department d) {
		for (Treatment t : AbstractTreatment.allTreatments) {
			if (t.getName().equals(string)) {
				d.addTreatment(t);
			}
		}

	}

	private void personForDepartment() throws SQLException {
		String sql = "SELECT COUNT(D.name) as DB,D.id as did,D.name as name,P.last_name as lname,P.first_name as fname FROM department D INNER JOIN person P ON P.department_id=D.id GROUP BY D.name";
		Statement stm = (Statement) this.connect.createStatement();
		ResultSet rs = stm.executeQuery(sql);
		while (rs.next()) {
			if (rs.getInt("DB") == 1) {
				this.createDepartmentForPerson(rs);
			} else {
				this.findPersonByDepartmentId(rs.getInt("did"));
			}

		}
	}

	private void findPersonByDepartmentId(int id) throws SQLException {
		String sql = "SELECT * from person where department_id=" + id;
		List<Staff> staff = new ArrayList<>();
		List<Patient> patient = new ArrayList<>();
		Statement statement2 = (Statement) this.connect.createStatement();
		ResultSet resultSet2 = statement2.executeQuery(sql);
		while (resultSet2.next()) {
			for (Person p : AbstractPerson.persons) {
				if (p.getFirstName().equals(resultSet2.getString("first_name"))
						&& p.getLastName().equals(resultSet2.getString("last_name"))) {
					if (p instanceof Patient) {
						patient.add((Patient) p);
					}
					if (p instanceof Staff) {
						staff.add((Staff) p);
					}
				}
			}
		}
		this.findDepartmentById(id).setStaff(staff);
		this.findDepartmentById(id).setPatients(patient);

	}

	private void createDepartmentForPerson(ResultSet resultSet2) throws SQLException {
		for (Department d : Department.departments) {
			if (d.getName().equals(resultSet2.getString("name"))) {
				this.putInTheCorresspondingList(this.searchPerson(resultSet2), d);
			}
		}
	}

	private void putInTheCorresspondingList(Person searchPerson, Department d) {
		if (searchPerson instanceof Patient) {
			d.addPatient((Patient) searchPerson);
		}
		if (searchPerson instanceof Staff) {
			d.addStaff((Staff) searchPerson);
		}

	}

	private Person searchPerson(ResultSet resultSet2) throws SQLException {
		for (Person p : AbstractPerson.persons) {
			if (p.getFirstName().equals(resultSet2.getString("fname"))
					&& p.getLastName().equals(resultSet2.getString("lname"))) {
				return p;
			}
		}
		return null;
	}

	@Override
	public <T extends Person> List<T> findPersonsFromDepartment(T type, Department department) throws SQLException {
		return null;
	}

	public void initDb() throws Exception {
		this.restoreDB();
		// this.statement = (Statement) this.connect.createStatement();
		// this.resultSet = this.statement.executeQuery("show databases");
		// while (this.resultSet.next()) {
		// String tableName = this.resultSet.getString(COL_DATABASE_NAME);
		// if (tableName.equalsIgnoreCase(DATABASE)) {
		// // return;
		// }
		// }
	}

	public boolean restoreDB() {
		Path p = Paths.get(PATH);
		String path = p.toString() + "\\" + BACKUP_SQL;
		String[] executeCmd = new String[] { "mysql", "--user=" + USERNAME, "-e", "source " + path };
		Process runtimeProcess;
		try {

			runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();

			if (processComplete == 0) {
				System.out.println("Backup restored successfully");
				return true;
			} else {
				System.out.println("Could not restore the backup");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}

	/*
	 * public void backupFromSQL() throws Exception { URI uri =
	 * this.getClass().getClassLoader().getResource(BACKUP_SQL).toURI(); Path path =
	 * Paths.get(uri); try (Scanner read = new Scanner(new File(path.toString()))) {
	 * read.useDelimiter(";"); while (read.hasNext()) { String sql = read.next();
	 * this.statement.executeUpdate(read.next()); } } catch (Exception e) {
	 * e.printStackTrace(); } }
	 */

	public void close() {
		try {
			this.backupDB();
			if (this.resultSet != null) {
				this.resultSet.close();
			}
			if (this.statement != null) {
				this.statement.close();
			}
			if (this.connect != null) {
				this.connect.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
