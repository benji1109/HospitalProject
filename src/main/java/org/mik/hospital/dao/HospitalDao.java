/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.dao;

import java.sql.SQLException;
import java.util.List;

import org.mik.hospital.department.Department;
import org.mik.hospital.disease.diagnosis.Diagnosis;
import org.mik.hospital.disease.symptoms.Symptoms;
import org.mik.hospital.person.Person;
import org.mik.hospital.person.patient.Patient;
import org.mik.hospital.treatment.Treatment;

/**
 * @author Burka Benjamin
 *
 */
public interface HospitalDao {
	public static final String INJURY = "Injury";
	public static final String BACTERIA = "Bacteria";
	public static final String VIRUS = "Virus";
	public static final String SYMPTOMS = "symptoms";
	public static final String INFORMATION_SCHEMA = "SELECT * FROM information_schema.tables";
	public static final String TABLE_NAME = "TABLE_NAME";
	public static final String PATH = "src\\main\\resources\\";
	public static final String URL_AND_PORT = "jdbc:mysql://localhost:3306/hospital?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	public static final String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";
	public static final String PASSWORD = "";
	public static final String USERNAME = "root";
	public static final String DATABASE = "hospital";
	public static final String COL_DATABASE_NAME = "database";
	public static final String BACKUP_SQL = "backup.sql";

	public void findAllSymptoms() throws SQLException;

	void findAllDiagnosis() throws SQLException;

	public void findAllPerson() throws Exception;

	public void findAllTreatment() throws SQLException;

	public Patient findPatientwithDisease(Diagnosis diagnosis) throws SQLException;

	public Diagnosis findDiagnosiswithSymptoms(List<Symptoms> symptoms) throws SQLException;

	public Treatment findTreatmentforDiagnosis(Diagnosis diagnosis) throws SQLException;

	public Department findDepartmentforPatient(Patient patient) throws SQLException;

	public Department findDepartmentforTreatment(Treatment treatment) throws SQLException;

	public <T extends Person> List<T> findPersonsFromDepartment(T type, Department department) throws SQLException;// generic

}
