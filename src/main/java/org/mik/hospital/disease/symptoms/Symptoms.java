/**
 *Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease.symptoms;

import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.disease.AbstractDisease;

/**
 * Symptoms final class, constans for databases
 *
 * @author Burka Benjamin
 *
 */
public final class Symptoms extends AbstractDisease {
	public final static String TABLE_NAME = "symptoms";
	public final static String COL_ID = "id";
	public final static String COL_NAME = "name";

	public static List<Symptoms> allSymptoms = new ArrayList<>();

	/**
	 * constructor
	 *
	 * @param name
	 */
	public Symptoms(String name) {
		super(name);
		// TODO if contains already
		allSymptoms.add(this);
	}

	/**
	 * @param rs
	 * @throws SQLException
	 */
	// public Symptoms(ResultSet rs) throws SQLException {
	// super(rs);
	// allSymptoms.add(this);
	// }
	/**
	 * true-ra állítja az isSymptom alap false értékét
	 */
	@Override
	public boolean isSymptom() {
		return true;
	}

	@Override
	public String toString() {
		return "Symptoms [" + this.getName() + "]";
	}

}
