/**
 *Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease;

/**
 * Interface for Disease , constant for database
 *
 * @author Burka Benjamin
 */
public interface Disease {

	public final static String COL_NAME = "name";

	public boolean isBacteria();

	public boolean isInjury();

	public boolean isVirus();

	public boolean isDiagnosis();

	public boolean isSymptom();

	public String getName();

}
