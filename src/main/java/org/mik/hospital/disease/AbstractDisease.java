/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for diseases
 *
 * @author Burka Benjamin
 */
public abstract class AbstractDisease implements Disease {
	private String name;
	public static List<Disease> diseases = new ArrayList<>();

	/**
	 * constructor , hozzáadja a diseases listához
	 *
	 * @param name
	 */
	public AbstractDisease(String name) {
		super();
		this.name = name;
		diseases.add(this);
	}

	/**
	 * Resultsetes constructor
	 *
	 * @param rs
	 * @throws SQLException
	 */
	public AbstractDisease(ResultSet rs) throws SQLException {
		super();
		this.name = rs.getString(Disease.COL_NAME);
		diseases.add(this);
	}

	@Override
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean isBacteria() {
		return false;
	}

	@Override
	public boolean isInjury() {
		return false;
	}

	@Override
	public boolean isVirus() {
		return false;
	}

	@Override
	public boolean isDiagnosis() {
		return false;
	}

	@Override
	public boolean isSymptom() {
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AbstractDisease other = (AbstractDisease) obj;
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AbstractDisease [name=" + this.name + "]";
	}

}
