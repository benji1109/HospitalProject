/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease.diagnosis.bacteria;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.disease.diagnosis.AbstractDiagnosis;
import org.mik.hospital.disease.diagnosis.Seriousness;
import org.mik.hospital.disease.symptoms.Symptoms;

/**
 * final class Bacteria
 * 
 * @author Burka Benjamin
 */
public final class Bacteria extends AbstractDiagnosis {
	public static List<Bacteria> bacterias = new ArrayList<>();

	/**
	 * @param name
	 * @param seriousness
	 * @param symptoms
	 *            hozzáadja a bacterias listához
	 */
	public Bacteria(String name, Seriousness seriousness, List<Symptoms> symptoms) {
		super(name, seriousness, symptoms);
		bacterias.add(this);
	}

	/**
	 * ResultSet Constructor
	 *
	 * @param rs
	 * @param symptoms
	 * @throws SQLException
	 *             hozzáadja a bacterias listához
	 */
	public Bacteria(ResultSet rs, List<Symptoms> symptoms) throws SQLException {
		super(rs, symptoms);
		bacterias.add(this);
	}

	@Override
	public boolean isBacteria() {
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " baktérium vagyok\n";
	}
}
