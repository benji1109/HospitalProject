/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease.diagnosis;

import java.util.List;

import org.mik.hospital.disease.Disease;
import org.mik.hospital.disease.symptoms.Symptoms;

/**
 * Interface Diagnosis , constans for database
 *
 * @author Burka Benjamin
 *
 */
public interface Diagnosis extends Disease {
	public final static String TABLE_NAME = "diagnosis";
	public final static String COL_ID = "id";
	public final static String COL_NAME = "name";
	public final static String COL_SERIOUSNESS = "seriousness";
	public final static String COL_TYPE = "type";

	public Seriousness getSeriousness();

	public List<Symptoms> getSymptoms();
}
