/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease.diagnosis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.disease.AbstractDisease;
import org.mik.hospital.disease.symptoms.Symptoms;

/**
 * abstract class for Diagnosises
 * 
 * @author Burka Benjamin
 */
public abstract class AbstractDiagnosis extends AbstractDisease implements Diagnosis {
	public static List<Diagnosis> alldiagnosis = new ArrayList<>();
	private Seriousness seriousness;
	private List<Symptoms> symptoms;

	/**
	 * Constructor , hozzáadja az alldiagnosis listához
	 *
	 * @param name
	 * @param seriousness
	 * @param symptoms
	 */
	public AbstractDiagnosis(String name, Seriousness seriousness, List<Symptoms> symptoms) {
		super(name);
		this.seriousness = seriousness;
		this.symptoms = symptoms;
		alldiagnosis.add(this);
	}

	/**
	 * Constructor ResultSet
	 *
	 * @param rs
	 * @throws SQLException
	 */
	public AbstractDiagnosis(ResultSet rs, List<Symptoms> symptoms) throws SQLException {
		super(rs);
		this.seriousness = Seriousness.fromInt(rs.getInt(Diagnosis.COL_SERIOUSNESS));
		this.symptoms = symptoms;
		alldiagnosis.add(this);
	}

	/**
	 * Megadja a betegség súlyosságát
	 */
	@Override
	public Seriousness getSeriousness() {
		return this.seriousness;
	}

	public void setSeriousness(Seriousness seriousness) {
		this.seriousness = seriousness;
	}

	/**
	 * listába kigyűjti a tüneteket
	 *
	 * @return
	 */
	@Override
	public List<Symptoms> getSymptoms() {
		return this.symptoms;
	}

	/**
	 * symptoms to set
	 *
	 * @param symptoms
	 */
	public void setSymptoms(List<Symptoms> symptoms) {
		this.symptoms = symptoms;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.seriousness == null) ? 0 : this.seriousness.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AbstractDiagnosis other = (AbstractDiagnosis) obj;
		if (this.seriousness != other.seriousness) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AbstractDiagnosis [seriousness=" + this.seriousness + ", symptoms=" + this.symptoms + "]";
	}

}
