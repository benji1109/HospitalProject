/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease.diagnosis;

/**
 * Enum Seriousness
 *
 * @author Burka Benjamin
 */
public enum Seriousness {
	LEVEL_1, LEVEL_2, LEVEL_3, UNKNOWN;
	/**
	 * Int értékből visszaadja a Seriousness-t
	 *
	 * @param val
	 * @return
	 */
	public static Seriousness fromInt(int val) {
		switch (val) {
		case 0:
			return LEVEL_1;
		case 1:
			return LEVEL_2;
		case 2:
			return LEVEL_3;
		default:
			return UNKNOWN;
		}
	}
}
