/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.patient;

import java.time.LocalDate;

import org.mik.hospital.person.BloodType;
import org.mik.hospital.person.PatientType;
import org.mik.hospital.person.Person;

/**
 * @author Benji
 *
 */
public interface Patient extends Person {

	public LocalDate getDateOfDiagnosis();

	public BloodType getBloodType();

	public PatientType getPatientType();

}
