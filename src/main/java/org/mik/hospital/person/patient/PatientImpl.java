/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.patient;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.disease.symptoms.Symptoms;
import org.mik.hospital.person.AbstractPerson;
import org.mik.hospital.person.Adress;
import org.mik.hospital.person.BloodType;
import org.mik.hospital.person.PatientType;

/**
 * @author Benji Final class Patient
 */
public final class PatientImpl extends AbstractPerson implements Patient {

	private LocalDate dateOfDiagnosis;
	private List<Symptoms> symptoms;
	private PatientType patientType;
	public static List<Patient> patients = new ArrayList<>();

	/**
	 * @param adress
	 * @param birtDate
	 * @param firstName
	 * @param lastName
	 * @param bloodType
	 * @param dateOfDiagnosis
	 * @param symptoms
	 * @param patientType
	 *            Patient constructor extended
	 */
	public PatientImpl(Adress adress, LocalDate birtDate, String firstName, String lastName, BloodType bloodType,
			LocalDate dateOfDiagnosis, List<Symptoms> symptoms, PatientType patientType) {
		super(adress, birtDate, firstName, lastName, bloodType);
		this.dateOfDiagnosis = dateOfDiagnosis;
		this.symptoms = symptoms;
		this.patientType = patientType;
		patients.add(this);
	}

	/**
	 * Patient Constructor with the SQL Parameters
	 *
	 * @param rs
	 * @param adress
	 * @throws Exception
	 */
	public PatientImpl(ResultSet rs, Adress adress) throws Exception {
		super(rs, adress);
		this.dateOfDiagnosis = null;
		this.symptoms = null;
		this.patientType = null;
		patients.add(this);
		// TODO + oszlop létrehozása , DateofDiagnosis
	}

	/**
	 * LocalDate típusba visszaadja a diagnosis dátumot
	 *
	 * @return the dateOfDiagnosis
	 */
	@Override
	public LocalDate getDateOfDiagnosis() {
		return this.dateOfDiagnosis;
	}

	/**
	 * @param dateOfDiagnosis
	 *            the dateOfDiagnosis to set
	 */
	public void setDateOfDiagnosis(LocalDate dateOfDiagnosis) {
		this.dateOfDiagnosis = dateOfDiagnosis;
	}

	/**
	 * Egy listába kigyűjti a tüneteket
	 *
	 * @return the symptoms
	 */
	public List<Symptoms> getSymptoms() {
		return this.symptoms;
	}

	/**
	 * @param symptoms
	 *            the symptoms to set,with the Symptoms List
	 */
	public void setSymptoms(List<Symptoms> symptoms) {
		this.symptoms = symptoms;
	}

	/**
	 * PatientType értéket ad vissza megadja a beteg típusát
	 *
	 * @return the patientType
	 */
	@Override
	public PatientType getPatientType() {
		return this.patientType;
	}

	/**
	 * @param patientType
	 *            the patientType to set
	 */
	public void setPatientType(PatientType patientType) {
		this.patientType = patientType;
	}

	/**
	 * Felülírja az isPatient függvényt értékét false-ról true-ra állítja
	 */
	@Override
	public boolean isPatient() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.dateOfDiagnosis == null) ? 0 : this.dateOfDiagnosis.hashCode());
		result = prime * result + ((this.patientType == null) ? 0 : this.patientType.hashCode());
		result = prime * result + ((this.symptoms == null) ? 0 : this.symptoms.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		PatientImpl other = (PatientImpl) obj;
		if (this.dateOfDiagnosis == null) {
			if (other.dateOfDiagnosis != null) {
				return false;
			}
		} else if (!this.dateOfDiagnosis.equals(other.dateOfDiagnosis)) {
			return false;
		}
		if (this.patientType != other.patientType) {
			return false;
		}
		if (this.symptoms == null) {
			if (other.symptoms != null) {
				return false;
			}
		} else if (!this.symptoms.equals(other.symptoms)) {
			return false;
		}
		return true;
	}

}
