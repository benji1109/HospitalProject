/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Benji
 *	class Adress
 */
public class Adress {
	public final static String TABLE_NAME = "adress";
	public final static String COL_ID = "id";
	public final static String COL_COUNTRY = "country";
	public final static String COL_STATE = "state";
	public final static String COL_CITY = "city";
	public final static String COL_POSTALCODE = "postalcode";
	public final static String COL_STREET="street";
	
	private String country;
	private String state;
	private String city;
	private Integer postalCode;
	private String street;
	private static List<Adress>adresses = new ArrayList<>();

	/**
	 * 
	 * @param country
	 * @param state
	 * @param city
	 * @param postalCode
	 * @param street
	 * Hozzáadja az adresses listához
	 */
	public Adress(String country, String state, String city, Integer postalCode, String street) {
		super();
		this.country = country;
		this.state = state;
		this.city = city;
		this.postalCode = postalCode;
		this.street = street;
		adresses.add(this);
	}
	/**
	 * ResultSet constructor
	 * @param rs
	 * @throws SQLException
	 */
	public Adress(ResultSet rs) throws SQLException {
		super();
		this.country = rs.getString(Adress.COL_COUNTRY);
		this.state =  rs.getString(Adress.COL_STATE);
		this.city =  rs.getString(Adress.COL_CITY);
		this.postalCode =  Integer.valueOf(rs.getInt(Adress.COL_POSTALCODE));
		this.street =  rs.getString(Adress.COL_STREET);
		adresses.add(this);
	}

	
	/**
	 * String típusban visszaadja az országot
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Visszaadja az államot
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Visszaadja a várost
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * 
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Visszaadja az irányítószámot
	 * @return the postalCode
	 */
	public Integer getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Visszaadja az utcanevet
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Adress other = (Adress) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Adress country=" + country + ", state=" + state + ", city=" + city + ", postalCode=" + postalCode
				+ ", street=" + street;
	}

}
