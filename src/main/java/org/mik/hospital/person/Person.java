/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person;

import java.time.LocalDate;

/**
 * @author Benji Person interface , constans for database
 */
public interface Person {
	public final static String TABLE_NAME = "person";
	public final static String COL_ID = "id";
	public final static String COL_FIRST_NAME = "first_name";
	public final static String COL_LAST_NAME = "last_name";
	public final static String COL_BLOODTYPE = "bloodtype";
	public final static String COL_BIRTH_DATE = "birth_date";
	public final static String COL_ADRESS_ID = "adress_id";
	public final static String COL_SALARY = "salary";
	public final static String COL_PHONE_NUMBER = "phone_number";
	public final static String COL_NUMBER_OF_PATIENT = "number_of_patient";
	public final static String COL_SPECIALIZATION = "specialization";
	public final static String COL_DEPARTMENT_ID = "department_id";

	Adress getAdress();

	LocalDate getBirtDate();

	String getFullName();

	String getFirstName();

	String getLastName();

	public BloodType getBloodType();

	boolean isDoctor();

	boolean isStaff();

	boolean isNurse();

	boolean isReceptionist();

	boolean isPatient();

}
