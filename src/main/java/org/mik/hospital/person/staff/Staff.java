/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff;

import org.mik.hospital.person.Person;

/**
 * @author Benji
 *	Staff interface
 */
public interface Staff extends Person {

	public Integer getSalary();

}
