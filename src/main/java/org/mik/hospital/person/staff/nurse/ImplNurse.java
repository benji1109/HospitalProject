/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff.nurse;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.person.Adress;
import org.mik.hospital.person.BloodType;
import org.mik.hospital.person.staff.AbstractStaff;

/**
 * @author Benji final class Nurse
 */
public final class ImplNurse extends AbstractStaff implements Nurse {

	private Integer numberOfPatient;
	public static List<Nurse> nurses = new ArrayList<>();

	/**
	 * @param adress
	 * @param birtDate
	 * @param firstName
	 * @param lastName
	 * @param bloodType
	 * @param salary
	 * @param numberOfPatient
	 *            extended constructor
	 */
	public ImplNurse(Adress adress, LocalDate birtDate, String firstName, String lastName, BloodType bloodType,
			Integer salary, Integer numberOfPatient) {
		super(adress, birtDate, firstName, lastName, bloodType, salary);
		this.numberOfPatient = numberOfPatient;
		nurses.add(this);
	}

	/**
	 * ResultSet constructor
	 * 
	 * @param rs
	 * @param adress
	 * @throws Exception
	 */
	public ImplNurse(ResultSet rs, Adress adress) throws Exception {
		super(rs, adress);
		this.numberOfPatient = rs.getInt(Nurse.COL_NUMBER_OF_PATIENT);
		nurses.add(this);
	}

	/**
	 * Integer típúsban visszaadja a nővérhez tartozó páciensek számát
	 * 
	 * @return the numberOfPatient
	 */
	@Override
	public Integer getNumberOfPatient() {
		return this.numberOfPatient;
	}

	/**
	 * @param numberOfPatient
	 *            the numberOfPatient to set
	 */
	public void setNumberOfPatient(Integer numberOfPatient) {
		this.numberOfPatient = numberOfPatient;
	}

	/**
	 * felülírja az isNurse() függvényt az alap false értéket true-ra alakitja
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see org.mik.hospital.person.AbstractPerson#isNurse()
	 */
	@Override
	public boolean isNurse() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.numberOfPatient == null) ? 0 : this.numberOfPatient.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ImplNurse other = (ImplNurse) obj;
		if (this.numberOfPatient == null) {
			if (other.numberOfPatient != null) {
				return false;
			}
		} else if (!this.numberOfPatient.equals(other.numberOfPatient)) {
			return false;
		}
		return true;
	}

}
