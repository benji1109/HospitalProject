/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff.receptioner;

import org.mik.hospital.person.staff.Staff;

/**
 * @author Benji
 *	Receptioner interface
 */
public interface Receptioner extends Staff {

	public Integer getPhoneNumber();

}
