/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff.doctor;

/**
 * @author Benji
 *	Enum a specialization-nak
 */
public enum Specialization {
	NEUROLOGIST, RADIOLOGIST, GENERAL_SURGEON, INTERN,RESIDENT,UNKNOWN;

	/**
	 * Int típúsú értékből Specialization értéket ad vissza
	 * @param val
	 * @return
	 */
	public static Specialization fromInt(int val) {
		switch(val) {
		case 1 :
			return NEUROLOGIST;
		case 2 : 
			return RADIOLOGIST;
		case 3 :
			return GENERAL_SURGEON;
		case 4:
			return RESIDENT;
		default :
			return UNKNOWN;
		}
	}



}

