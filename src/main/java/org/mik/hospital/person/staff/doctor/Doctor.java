/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff.doctor;

import org.mik.hospital.person.BloodType;
import org.mik.hospital.person.staff.Staff;

/**
 * @author Benji
 *	Doctor interface
 */
public interface Doctor extends Staff {

	public Specialization getSpecialization();

	public BloodType getBloodType();

}
