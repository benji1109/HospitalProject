/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff.nurse;

import org.mik.hospital.person.BloodType;
import org.mik.hospital.person.staff.Staff;

/**
 * @author Benji
 *	Nurse interface
 */
public interface Nurse extends Staff {

	public Integer getNumberOfPatient();

	public BloodType getBloodType();

}
