/**
 *
 */
package org.mik.hospital.person;

/**
 * @author Benji
 *	enum PatientType
 */
public enum PatientType {
	INPATIENT, OUTPATIENT
}
