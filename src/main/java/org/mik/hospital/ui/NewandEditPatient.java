package org.mik.hospital.ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class NewandEditPatient {

	private JFrame frmNewPatient;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewandEditPatient window = new NewandEditPatient();
					window.frmNewPatient.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NewandEditPatient() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNewPatient = new JFrame();
		frmNewPatient.setTitle("Patient");
		frmNewPatient.setBounds(100, 100, 640, 480);
		frmNewPatient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNewPatient.getContentPane().setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name :");
		lblFirstName.setHorizontalAlignment(SwingConstants.CENTER);
		lblFirstName.setBounds(10, 42, 78, 14);
		frmNewPatient.getContentPane().add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last Name :");
		lblLastName.setHorizontalAlignment(SwingConstants.CENTER);
		lblLastName.setBounds(335, 42, 78, 14);
		frmNewPatient.getContentPane().add(lblLastName);
		
		JLabel lblBirthDate = new JLabel("Birth Date (YYYY-MM-DD) :");
		lblBirthDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblBirthDate.setBounds(0, 76, 161, 27);
		frmNewPatient.getContentPane().add(lblBirthDate);
		
		JLabel lblBloodType = new JLabel("BloodType :");
		lblBloodType.setHorizontalAlignment(SwingConstants.CENTER);
		lblBloodType.setBounds(335, 82, 78, 14);
		frmNewPatient.getContentPane().add(lblBloodType);
		
		JLabel lblAdress_country = new JLabel("Country :");
		lblAdress_country.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdress_country.setBounds(10, 143, 78, 27);
		frmNewPatient.getContentPane().add(lblAdress_country);
		
		JLabel lblAdress_state = new JLabel("State :");
		lblAdress_state.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdress_state.setBounds(335, 143, 78, 27);
		frmNewPatient.getContentPane().add(lblAdress_state);
		
		JLabel lblAdress_city = new JLabel("City :");
		lblAdress_city.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdress_city.setBounds(10, 181, 78, 27);
		frmNewPatient.getContentPane().add(lblAdress_city);
		
		JLabel lblPostalCode = new JLabel("Postal code :");
		lblPostalCode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPostalCode.setBounds(335, 187, 78, 14);
		frmNewPatient.getContentPane().add(lblPostalCode);
		
		JLabel lblStreet = new JLabel("Street :");
		lblStreet.setHorizontalAlignment(SwingConstants.CENTER);
		lblStreet.setBounds(10, 219, 78, 14);
		frmNewPatient.getContentPane().add(lblStreet);
		
		textField = new JTextField();
		textField.setBounds(423, 184, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(158, 216, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(158, 146, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(158, 184, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(423, 39, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(158, 79, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(158, 39, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(423, 146, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(423, 79, 152, 20);
		frmNewPatient.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("SAVE");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainUI nw= new MainUI();
				frmNewPatient.setVisible(false);
				nw.main(null);
			}
		});
		btnNewButton.setBounds(62, 357, 89, 23);
		frmNewPatient.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("DISCARD");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainUI nw= new MainUI();
				frmNewPatient.setVisible(false);
				nw.main(null);
			}
		});
		btnNewButton_1.setBounds(428, 357, 89, 23);
		frmNewPatient.getContentPane().add(btnNewButton_1);
	}
}
