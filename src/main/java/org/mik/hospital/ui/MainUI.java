package org.mik.hospital.ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.mik.hospital.dao.HospitalDaoImpl;
import org.mik.hospital.department.Department;
import org.mik.hospital.disease.diagnosis.AbstractDiagnosis;
import org.mik.hospital.disease.diagnosis.Diagnosis;
import org.mik.hospital.disease.diagnosis.bacteria.Bacteria;
import org.mik.hospital.disease.diagnosis.injuries.Injuries;
import org.mik.hospital.disease.diagnosis.virus.Virus;
import org.mik.hospital.disease.symptoms.Symptoms;
import org.mik.hospital.person.AbstractPerson;
import org.mik.hospital.person.Person;
import org.mik.hospital.person.patient.Patient;
import org.mik.hospital.person.patient.PatientImpl;
import org.mik.hospital.person.staff.doctor.Doctor;
import org.mik.hospital.person.staff.doctor.ImplDoctor;
import org.mik.hospital.person.staff.nurse.ImplNurse;
import org.mik.hospital.person.staff.nurse.Nurse;
import org.mik.hospital.person.staff.receptioner.ImplReceptioner;
import org.mik.hospital.person.staff.receptioner.Receptioner;
import org.mik.hospital.treatment.AbstractTreatment;
import org.mik.hospital.treatment.Treatment;
import org.mik.hospital.treatment.medicine.Medicine;
import org.mik.hospital.treatment.surgeon.Surgeon;

public class MainUI {

	private JFrame frmHospital;
	private JTable table;

	private DefaultTableModel model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainUI window = new MainUI();
					window.frmHospital.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainUI() {
		this.initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			HospitalDaoImpl.getInstance().readDataBase();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.frmHospital = new JFrame();
		this.frmHospital.setTitle("Hospital");
		this.frmHospital.setBounds(100, 100, 640, 480);
		this.frmHospital.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frmHospital.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(52, 23, 544, 323);
		this.frmHospital.getContentPane().add(scrollPane);

		this.table = new JTable();
		scrollPane.setViewportView(this.table);

		JButton btnNewButton = new JButton("EDIT");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				NewandEditPatient nw = new NewandEditPatient();
				MainUI.this.frmHospital.setVisible(false);
				nw.main(null);

			}
		});
		btnNewButton.setBounds(62, 357, 89, 23);
		this.frmHospital.getContentPane().add(btnNewButton);

		JButton btnNewButton_1 = new JButton("DELETE");
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int dialogResult = JOptionPane.showConfirmDialog(null, "Would You Like to Delete the Current Row?");
				if (dialogResult == JOptionPane.YES_OPTION) {
					// MainUI.this.model
					// .removeRow(MainUI.this.table.convertRowIndexToModel(MainUI.this.table.getSelectedRow()));
					// Object o = MainUI.this.table.getValueAt(MainUI.this.table.getSelectedRow(),
					// MainUI.this.table.getSelectedColumn());
					//
					// try {
					// HospitalDaoImpl.getInstance().deletePersonById(Integer.valueOf(o.toString()));
					// } catch (NumberFormatException e1) {
					// // TODO Auto-generated catch block
					// e1.printStackTrace();
					// } catch (SQLException e1) {
					// // TODO Auto-generated catch block
					// e1.printStackTrace();
					// }

					// try {
					// HospitalDaoImpl.getInstance()
					// .deletePersonById((int) MainUI.this.model.getValueAt(
					// Integer.valueOf(MainUI.this.table.getSelectedRow()),
					// Integer.valueOf(MainUI.this.table.getSelectedColumn())));
					// } catch (SQLException e1) {
					// e1.printStackTrace();
					// }

				}
			}
		});
		btnNewButton_1.setBounds(201, 357, 89, 23);
		this.frmHospital.getContentPane().add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("NEW");
		btnNewButton_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				NewandEditPatient nw = new NewandEditPatient();
				MainUI.this.frmHospital.setVisible(false);
				nw.main(null);
			}
		});
		btnNewButton_2.setBounds(342, 357, 89, 23);
		this.frmHospital.getContentPane().add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("SORT");
		btnNewButton_3.setBounds(485, 357, 89, 23);
		this.frmHospital.getContentPane().add(btnNewButton_3);

		JMenuBar menuBar = new JMenuBar();
		this.frmHospital.setJMenuBar(menuBar);

		JMenu mnListAll = new JMenu("List all");
		menuBar.add(mnListAll);

		JMenu mnPerson = new JMenu("Person");
		mnPerson.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				MainUI.this.model = new DefaultTableModel(
						new String[] { "Vezetéknév:", "Keresztnév:", "Születésnap:", "Vértípus", "Ország:", "Beteg?" },
						0);

				for (Person p : AbstractPerson.persons) {
					MainUI.this.model.addRow(new Object[] { p.getLastName(), p.getFirstName(), p.getBirtDate(),
							p.getBloodType(), p.getAdress().getCountry(), (p.isPatient() ? "Igen" : "Nem") });
				}
				MainUI.this.table.setModel(MainUI.this.model);
				// try {
				// HospitalDaoImpl.getInstance().createConnection();
				// MainUI.this.table.setModel(buildTableModel(HospitalDaoImpl.getInstance().findAllPersons()));
				// } catch (SQLException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// } catch (ClassNotFoundException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

			}
		});
		mnListAll.add(mnPerson);

		JMenu mnNewMenu = new JMenu("Patient");
		mnNewMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				MainUI.this.model = new DefaultTableModel(new String[] { "Vezetéknév:", "Keresztnév:", "Születésnap:",
						"Vértípus", "Ország:", "Diagnózis dátuma:" }, 0);

				for (Patient p : PatientImpl.patients) {
					MainUI.this.model.addRow(new Object[] { p.getLastName(), p.getFirstName(), p.getBirtDate(),
							p.getBloodType(), p.getAdress().getCountry(), p.getDateOfDiagnosis() });
				}

				MainUI.this.table.setModel(MainUI.this.model);
			}
		});
		mnPerson.add(mnNewMenu);

		JMenuItem mntmInpatient = new JMenuItem("InPatient");
		mntmInpatient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		mnNewMenu.add(mntmInpatient);

		JMenuItem mntmOutpatient = new JMenuItem("OutPatient");
		mnNewMenu.add(mntmOutpatient);

		JMenuItem mntmDoctor = new JMenuItem("Doctor");
		mntmDoctor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {
				MainUI.this.model = new DefaultTableModel(new String[] { "Vezetéknév:", "Keresztnév:", "Születésnap:",
						"Vértípus", "Ország:", "Fizetés:", "Specializáció" }, 0);

				for (Doctor p : ImplDoctor.doctors) {
					MainUI.this.model.addRow(new Object[] { p.getLastName(), p.getFirstName(), p.getBirtDate(),
							p.getBloodType(), p.getAdress().getCountry(), p.getSalary(), p.getSpecialization() });
				}

				MainUI.this.table.setModel(MainUI.this.model);
			}
		});
		mnPerson.add(mntmDoctor);

		JMenuItem mntmNurse = new JMenuItem("Nurse");
		mntmNurse.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				MainUI.this.model = new DefaultTableModel(new String[] { "Vezetéknév:", "Keresztnév:", "Születésnap:",
						"Vértípus", "Ország:", "Fizetés:", "Betgek száma:" }, 0);

				for (Nurse p : ImplNurse.nurses) {
					MainUI.this.model.addRow(new Object[] { p.getLastName(), p.getFirstName(), p.getBirtDate(),
							p.getBloodType(), p.getAdress().getCountry(), p.getSalary(), p.getNumberOfPatient() });
				}

				MainUI.this.table.setModel(MainUI.this.model);
			}
		});
		mnPerson.add(mntmNurse);

		JMenuItem mntmReceptionist = new JMenuItem("Receptionist");
		mntmReceptionist.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				MainUI.this.model = new DefaultTableModel(new String[] { "Vezetéknév:", "Keresztnév:", "Születésnap:",
						"Vértípus", "Ország:", "Fizetés:", "Telefonszám:" }, 0);

				for (Receptioner p : ImplReceptioner.receptioners) {
					MainUI.this.model.addRow(new Object[] { p.getLastName(), p.getFirstName(), p.getBirtDate(),
							p.getBloodType(), p.getAdress().getCountry(), p.getSalary(), p.getPhoneNumber() });
				}

				MainUI.this.table.setModel(MainUI.this.model);
			}
		});
		mnPerson.add(mntmReceptionist);

		JMenuItem mntmDepartment = new JMenuItem("Department");
		mntmDepartment.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent arg0) {

				MainUI.this.model = new DefaultTableModel(new String[] { "Osztály neve:" }, 0);

				for (Department d : Department.departments) {
					MainUI.this.model.addRow(new Object[] { d.getName() });
				}

				MainUI.this.table.setModel(MainUI.this.model);
			}
		});
		mnListAll.add(mntmDepartment);

		JMenu mnNewMenu_1 = new JMenu("Diagnosis");
		mnNewMenu_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				MainUI.this.model = new DefaultTableModel(new String[] { "Betegség megnevezése:", "Súlyosság:" }, 0);

				for (Diagnosis d : AbstractDiagnosis.alldiagnosis) {
					MainUI.this.model.addRow(new Object[] { d.getName(), d.getSeriousness() });
				}

				MainUI.this.table.setModel(MainUI.this.model);

			}
		});
		mnListAll.add(mnNewMenu_1);

		JMenuItem mntmVirus = new JMenuItem("Virus");
		mntmVirus.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				MainUI.this.model = new DefaultTableModel(new String[] { "Vírus megnevezése:", "Súlyosság:" }, 0);

				for (Virus d : Virus.virus) {
					MainUI.this.model.addRow(new Object[] { d.getName(), d.getSeriousness() });
				}

				MainUI.this.table.setModel(MainUI.this.model);
			}
		});
		mnNewMenu_1.add(mntmVirus);

		JMenuItem mntmInjury = new JMenuItem("Injury");
		mntmInjury.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				MainUI.this.model = new DefaultTableModel(new String[] { "Baleset megnevezése:", "Súlyosság:" }, 0);

				for (Injuries d : Injuries.injuries) {
					MainUI.this.model.addRow(new Object[] { d.getName(), d.getSeriousness() });
				}

				MainUI.this.table.setModel(MainUI.this.model);

			}
		});
		mnNewMenu_1.add(mntmInjury);

		JMenuItem mntmBacteria = new JMenuItem("Bacteria");
		mntmBacteria.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				MainUI.this.model = new DefaultTableModel(new String[] { "Baktérium megnevezése:", "Súlyosság:" }, 0);

				for (Bacteria d : Bacteria.bacterias) {
					MainUI.this.model.addRow(new Object[] { d.getName(), d.getSeriousness() });
				}

				MainUI.this.table.setModel(MainUI.this.model);
			}
		});
		mnNewMenu_1.add(mntmBacteria);

		JMenu mnThreatment = new JMenu("Treatment");
		mnThreatment.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {

				MainUI.this.model = new DefaultTableModel(new String[] { "Kezelés megnevezése:" }, 0);

				for (Treatment d : AbstractTreatment.allTreatments) {
					MainUI.this.model.addRow(new Object[] { d.getName() });
				}

				MainUI.this.table.setModel(MainUI.this.model);
			}
		});

		JMenuItem mntmSymptoms = new JMenuItem("Symptoms");
		mntmSymptoms.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				MainUI.this.model = new DefaultTableModel(new String[] { "Tünet megnevezés:" }, 0);

				for (Symptoms d : Symptoms.allSymptoms) {
					MainUI.this.model.addRow(new Object[] { d.getName() });
				}

				MainUI.this.table.setModel(MainUI.this.model);

			}
		});
		mnListAll.add(mntmSymptoms);
		mnListAll.add(mnThreatment);

		JMenuItem mntmSurgeon = new JMenuItem("Surgeon");
		mntmSurgeon.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				MainUI.this.model = new DefaultTableModel(new String[] { "Megnevezés:", "Testrész:" }, 0);

				for (Surgeon d : Surgeon.surgeons) {
					MainUI.this.model.addRow(new Object[] { d.getName(), d.getBodyPart() });
				}

				MainUI.this.table.setModel(MainUI.this.model);

			}
		});
		mnThreatment.add(mntmSurgeon);

		JMenuItem mntmMedicine = new JMenuItem("Medicine");
		mntmMedicine.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				MainUI.this.model = new DefaultTableModel(new String[] { "Megnevezés:", "Ár:" }, 0);

				for (Medicine d : Medicine.medicines) {
					MainUI.this.model.addRow(new Object[] { d.getName(), d.getPrice() });
				}

				MainUI.this.table.setModel(MainUI.this.model);

			}
		});
		mnThreatment.add(mntmMedicine);

		JMenu mnSearch = new JMenu("Search ");
		menuBar.add(mnSearch);

		JMenuItem mntmByName = new JMenuItem("by Name");
		mnSearch.add(mntmByName);

		JMenuItem mntmByDiagnosis = new JMenuItem("by Diagnosis");
		mnSearch.add(mntmByDiagnosis);

		JMenuItem mntmByDepartment = new JMenuItem("by Department");
		mnSearch.add(mntmByDepartment);
	}

	public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {

		java.sql.ResultSetMetaData metaData = rs.getMetaData();

		Vector<String> columnNames = new Vector<>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}

		Vector<Vector<Object>> data = new Vector<>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(rs.getObject(columnIndex));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}

}
